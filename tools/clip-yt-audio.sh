#!/bin/bash

# This script clips audio from a YouTube video into an audio file

#Arguments: URL, Time stamp -5 seconds, length of clip, audio file name

readarray -t urls <<< "$(yt-dlp --youtube-skip-dash-manifest -g "$1")"
ffmpeg -ss $2 -i "${urls[0]}" -ss $2 -i "${urls[1]}" -ss 5 -map 1:a -c:a pcm_s16le -ac 1 -t $3 $4
echo "Audio clip saved as $4"