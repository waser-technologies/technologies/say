#!/usr/bin/env zsh
banner=(
'   _____            '
'  / ___/____ ___  __'
'  \__ \/ __ `/ / / /'
' ___/ / /_/ / /_/ / '
'/____/\__,_/\__, /  '
'           /____/   '              
""
)
version="latest"

# $LANG >> $lang, $loc
i18n=($(echo $LANG | tr "." "\n"))
if test ${i18n[2]} != "UTF-8"; then
    echo "You must use UTF-8."
    exit 8
fi
l10n=($(echo ${i18n[1]} | tr "_" "\n"))
lang=${l10n[1]}
loc=${l10n[2]}
# fr_CH.UTF-8 >> fr, CH
# en_US.UTF-8 >> en, US

if [ $lang = "fr" ]; then
    GIT_MODEL="https://gitlab.com/waser-technologies/models/fr/say.git"
    MODEL_LANGUAGE="french"
else
    GIT_MODEL="https://gitlab.com/waser-technologies/models/en/say.git"
    MODEL_LANGUAGE="english"
fi

# prints centered: [ version number ]
print_version() {
    printf "%*s\n" $(( (${#version} + 4 + ${COLUMNS}) / 2))  "[ $version ]"
    echo -e "\n\n"
}

# prints centerd: $banner + version
print_banner() {
    echo -e "\n\n\n"
    for i in ${banner}; do
        printf "%*s\n" $(( (${#i} + ${COLUMNS}) / 2)) "$i"
    done
    echo -e "\n"
    print_version
}

#BTW do you arch ?
check_for_arch() {
    clear
    print_banner
    echo -e "\n"
    if [ -f "/etc/arch-release" ]; then
        release=$(cat '/etc/arch-release')
        echo "BTW, I use ${release}."
        sleep 0.5
    else
        echo "Assistant: You should use Arch Linux."
        exit 1
    fi
    echo -e "\n"
}

# How would you love me to install something without it ?
check_for_connectivity() {
    clear
    print_banner
    echo -e "\n"
    if ping -c 1 8.8.8.8 >/dev/null 2>&1; then
        echo "I'll just borrow your Internet connection for a while."
        sleep 0.5
    else
        echo "Assistant: Sorry Pal."
        echo "Assistant: You need Internet to install anything theses days."
        exit 2
    fi
}

print_banner
check_for_arch
check_for_connectivity

# This is a handy way to make the user wait nicely
. <(curl -sLo- "https://git.io/progressbar")

bar::start

# Recipe to install + some chit chat to make the wait more pleasant
StuffToDo=(
    "echo 'Preparing to say something.'"
    "sleep 2 && echo 'Wait it`s the first time'"
    "sleep 1 && echo 'Wow, I don`t know what to say'"
    "sleep 1 && echo 'Let me think about it'"
    "sleep 1 && echo 'In the meantime I´ll download everything I need'"
    "sleep 1 && echo 'Downloading files'"
    "sleep 1 && echo 'Downloading Say'"
    "git clone https://gitlab.com/waser-technologies/technologies/say.git /tmp/say"
    "echo 'Downloading ${MODEL_LANGUAGE} model for Say'"
    "git clone ${GIT_MODEL} /tmp/say/models"
    "echo 'Downloading dependencies'"
    "pip install -r /tmp/say/requirements.txt"
    "echo 'Copying files'"
    "sleep 1 && sudo cp -f /tmp/say/say /usr/bin/say && echo 'Copying files (1/8) [.       ]'"
    "sleep 1 && sudo cp -f /tmp/say/think /usr/bin/think && echo 'Copying files (2/8) [..      ]'"
    "sleep 1 && mkdir -p ~/.assistant/helpers/ && cp -f /tmp/say/data.helper ~/.assistant/helpers/say.helper && echo 'Copying files (3/8) [...     ]'"
    "sleep 1 && mkdir -p ~/.assistant/trainers/ && cp -f /tmp/say/models/model.train ~/.assistant/trainers/say.train && echo 'Copying files (4/8) [....    ]'"
    "sleep 1 && mkdir -p ~/.assistant/models/$lang/TTS/ && cp -f /tmp/say/models/*.json ~/.assistant/models/$lang/TTS/ && echo 'Copying files (5/8) [.....   ]'"
    "sleep 1 && cp -f /tmp/say/models/*.npy ~/.assistant/models/$lang/TTS/ && echo 'Copying files (6/8) [......  ]'"
    "sleep 1 && cp -f /tmp/say/models/*.pth.tar ~/.assistant/models/$lang/TTS/ && echo 'Copying files (7/8) [....... ]'"
    "sleep 1 && sudo cp -f /tmp/say/think.service /etc/systemd/user/think.service && echo 'Copying files (8/8) [........]'"
    "echo 'Removing old files'"
    "sleep 1 && trash -rf /tmp/say/"
    "sleep 1 && echo 'Enabling thinking services'"
    "sleep 1 && sudo systemctl enable --now think.service"
    "echo 'Installation complete'"
    "sleep 1 && echo 'Type ´say --help´ to get started'"
)

TotalSteps=${#StuffToDo[@]}

for Stuff in ${StuffToDo[@]}; do
    out=$(tail -c 47 <<< $(eval "${Stuff}" 2>>/var/log/say))
    clear
    print_banner    
    printf "%*s\n" $(( (${#out} + ${COLUMNS}) / 2)) "$out"
    StepsDone=$((${StepsDone:-0}+1))
    bar::status_changed $StepsDone $TotalSteps
done

bar::stop
