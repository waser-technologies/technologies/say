#!/usr/bin/env bash

# This script converts text files to speech using the `say` command

# say --version
# echo

dependencies=("sox" "bc" "say")

for dep in ${dependencies[@]}; do
    if ! command -v $dep &> /dev/null; then
        echo "Error: $dep is not installed"
        echo "Please install $dep and try again."
        exit 1
    fi
done


base_txt_path="./txt"
wav_path="./wav"

if [ ! -d $wav_path ]; then
    mkdir $wav_path
fi

# Get start time (for calculating time taken)
start_time=$(date +%s)

# For all dirs in base_txt_path
for dir in $base_txt_path/*; do
    dir_name=$(basename $dir)
    echo "Processing files in directory: $dir_name"
    # For all files in txt_path
    for txt_file in $base_txt_path/$dir_name/*; do
        # Get the base name of the file
        base_name=$(basename $txt_file)
        # Get the file name without extension
        file_name="${base_name%.*}"
        echo "Processing text file: $file_name"
        echo -n "Reading text file: $txt_file"
        # Get the content of the file
        content=$(cat $txt_file)
        echo
        # if output file exists, skip
        if [ -f "$wav_path/$dir_name/$file_name.wav" ]; then
            echo "Wav file already exists. Skipping..."
            echo
            continue
        fi
        mkdir -p $wav_path/$dir_name
        # Get the full path of the output file
        output_file="$wav_path/$dir_name/$file_name.wav"
        echo "Saving wav to: $output_file"
        echo "Converting text to speech:"
        # Say the text in the file and save it as a wav file
        say --out_path $output_file "${content}"
        echo
    done
    echo
done
echo

echo "All files have been converted to wav files"
echo
echo -n "Took $(($(date +%s) - $start_time)) seconds to produce "

wav_size_sec=$(find wav -name "*.wav" -exec soxi -D {} \; | awk '{sum += $1} END {print sum}')
if (( $(echo "$wav_size_sec < 60" | bc -l) )); then
    echo "$wav_size_sec seconds of audio"
elif (( $(echo "$wav_size_sec < 3600" | bc -l) )); then
    wav_size_min=$(echo "scale=2; $wav_size_sec / 60" | bc)
    echo "$wav_size_min minutes of audio"
else
    wav_size_hr=$(echo "scale=2; $wav_size_sec / 3600" | bc)
    echo "$wav_size_hr hours of audio"
fi
echo

